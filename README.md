# wifi-qrgen

`wifi-qrgen` outputs a QR Code (v10) with data formatted to provide access
information for a wireless network.

## Usage

```
Usage: wifi-qrgen -o output -s ssid [OPTIONS]

Options:
  -o <output>       Filename and relative path for output file
  -s <ssid>         SSID for network
  -l <logo>         Filename and relative path for logo image
  -b <background>   Filename and relative path for background image
  -p <password>     Password/passphrase for network
  --hide            Toggle accessability for hidden SSID (default "false").
```

## Dependencies

`wifi-qrgen` requires the following programs in order to function:
  - qrencode
  - ImageMagick

## Notes

For the optional logo and background images, it is strongly recommended to
only use files in PNG format.  For best results, a logo should be
monochromatic with a white outline and trimmed whereas a background should
avoid areas with white coloring.

The password/passphrase option should be avoided when the not passing in a
randomly generated password.  When a password is not provided as an argument,
the script will prompt for the value without revealing the plaintext.

## License

MIT
